package com.hocelot.lib.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.Signature;

import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;

import org.aspectj.lang.JoinPoint.StaticPart;
import org.aspectj.lang.reflect.SourceLocation;
import org.aspectj.runtime.internal.AroundClosure;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = TestConfiguration.class)
public class InOutAspectTest {

	@Autowired
	private FakeRestController controller;

	@Autowired
	private InOutAspect a;

	ProceedingJoinPoint pjp;
	
	ProceedingJoinPoint pjpException;


	@Before
	public void init() throws FileNotFoundException, IOException {


		pjp = new ProceedingJoinPoint() {

			public String toShortString() {
				// TODO Auto-generated method stub
				return null;
			}

			public String toLongString() {
				// TODO Auto-generated method stub
				return null;
			}

			public Object getThis() {
				// TODO Auto-generated method stub
				return null;
			}

			public Object getTarget() {

				return controller;
			}

			public StaticPart getStaticPart() {
				// TODO Auto-generated method stub
				return null;
			}

			public SourceLocation getSourceLocation() {
				// TODO Auto-generated method stub
				return null;
			}

			public Signature getSignature() {
				// TODO Auto-generated method stub
				return new Signature() {

					public String toShortString() {
						// TODO Auto-generated method stub
						return null;
					}

					public String toLongString() {
						// TODO Auto-generated method stub
						return null;
					}

					public String getName() {
						// TODO Auto-generated method stub
						return FakeController.class.getCanonicalName();
					}

					public int getModifiers() {
						// TODO Auto-generated method stub
						return 0;
					}

					public String getDeclaringTypeName() {
						// TODO Auto-generated method stub
						return null;

					}

					public Class getDeclaringType() {
						// TODO Auto-generated method stub
						return FakeController.class;
					}
				};
			}

			public String getKind() {
				// TODO Auto-generated method stub
				return null;
			}

			public Object[] getArgs() {

				return new Object[] { "arg1" };
			}

			public void set$AroundClosure(AroundClosure arg0) {
				// TODO Auto-generated method stub

			}

			public Object proceed(Object[] arg0) throws Throwable {
				// TODO Auto-generated method stub
				return null;
			}

			public Object proceed() throws Throwable {

				return "out";
			}
		};
		
		pjpException = new ProceedingJoinPoint() {

			public String toShortString() {
				// TODO Auto-generated method stub
				return null;
			}

			public String toLongString() {
				// TODO Auto-generated method stub
				return null;
			}

			public Object getThis() {
				// TODO Auto-generated method stub
				return null;
			}

			public Object getTarget() {

				return controller;
			}

			public StaticPart getStaticPart() {
				// TODO Auto-generated method stub
				return null;
			}

			public SourceLocation getSourceLocation() {
				// TODO Auto-generated method stub
				return null;
			}

			public Signature getSignature() {
				// TODO Auto-generated method stub
				return new Signature() {

					public String toShortString() {
						// TODO Auto-generated method stub
						return null;
					}

					public String toLongString() {
						// TODO Auto-generated method stub
						return null;
					}

					public String getName() {
						// TODO Auto-generated method stub
						return FakeController.class.getCanonicalName();
					}

					public int getModifiers() {
						// TODO Auto-generated method stub
						return 0;
					}

					public String getDeclaringTypeName() {
						// TODO Auto-generated method stub
						return null;

					}

					public Class getDeclaringType() {
						// TODO Auto-generated method stub
						return FakeController.class;
					}
				};
			}

			public String getKind() {
				// TODO Auto-generated method stub
				return null;
			}

			public Object[] getArgs() {

				return new Object[] { "arg1" };
			}

			public void set$AroundClosure(AroundClosure arg0) {
				// TODO Auto-generated method stub

			}

			public Object proceed(Object[] arg0) throws Throwable {
				// TODO Auto-generated method stub
				return null;
			}

			public Object proceed() throws Throwable {

				throw new RuntimeException("Error accesing");
			}
		};
	}

	@Test
	public void testAspect() throws Throwable {

		PrintWriter pw = new PrintWriter("/tmp/test.log");
		pw.close();
		
		//test
		a.logBefore(pjp);
		
		//read log
		byte[] encoded = Files.readAllBytes(Paths.get("/tmp/test.log"));
		String content = new String(encoded);
		
		encoded = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource("logExpected.log").getPath()));
		String contentExp = new String(encoded);
		
		
		Assert.assertEquals(
				contentExp,
				content);
	}
	
	@Test
	public void testAspectException() throws Throwable {

		PrintWriter pw = new PrintWriter("/tmp/test.log");
		pw.close();
		
		//test
		try{
			a.logBefore(pjpException);
		}catch (Throwable t){
			
		}
		
		//read log
		byte[] encoded = Files.readAllBytes(Paths.get("/tmp/test.log"));
		String content = new String(encoded);
		
		encoded = Files.readAllBytes(Paths.get(this.getClass().getClassLoader().getResource("logExpectedException.log").getPath()));
		String contentExp = new String(encoded);
		
		
		Assert.assertEquals(
				contentExp,
				content);
	}


}
