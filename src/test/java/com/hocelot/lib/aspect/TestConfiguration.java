package com.hocelot.lib.aspect;
import org.apache.log4j.PropertyConfigurator;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

import lombok.extern.log4j.Log4j;

@Configuration
@ComponentScan(basePackages="com.hocelot.lib.aspect")
@EnableAspectJAutoProxy(proxyTargetClass=true,exposeProxy=true)
@Log4j
public class TestConfiguration {

	@Bean(name="log up")
	public String configureLog(){
		System.setProperty("log4j.debug", ""); 
		PropertyConfigurator.configure(TestConfiguration.class.getResource("/log4j.properties"));
		//BasicConfigurator.configure();
		log.info("Init log");
		return "";
		
	}
	
}
