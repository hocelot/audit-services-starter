package com.hocelot.lib.aspect;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;

@Controller
public class FakeController {

	public ResponseEntity<String> companyGet(String companyInfoRequest)
			{
		return new ResponseEntity<String>("out",HttpStatus.OK);
	}
	
}
