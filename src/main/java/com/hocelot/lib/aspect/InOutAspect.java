package com.hocelot.lib.aspect;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.extern.log4j.Log4j;

@Aspect
@Log4j
public class InOutAspect {

	

	@Around("hocelotController()")
	public Object logBefore(ProceedingJoinPoint pjp) throws Throwable{
		String name = getAuth();
		Before b = auditBefore(pjp, name);
		try{
			Object ret = pjp.proceed();
			auditAfter(b, ret);
			return ret;
			
		}catch(Throwable t){
			auditException(b, t);
			throw t;
		}
		
		
		
	}

	private void auditException(Before b, Throwable t) throws JsonProcessingException {
		Throws th = new Throws(b.user,b.signature, t.getMessage(),"");
		log.info(mapper.writeValueAsString(th));
	}

	private void auditAfter(Before b, Object ret) throws JsonProcessingException {
		After a = new After(b.user,b.signature, ret);
		log.info(mapper.writeValueAsString(a));
	}

	private Before auditBefore(ProceedingJoinPoint pjp, String name) throws JsonProcessingException {
		Before b = new Before(name,pjp.getSignature().getDeclaringType().getCanonicalName()+"."+pjp.getSignature().getName(),pjp.getArgs());		
		log.info(mapper.writeValueAsString(b));
		return b;
	}

	private String getAuth() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		String name = authentication != null ? authentication.getName() : "";
		return name;
	}
	
	@Pointcut("hocelot() && controller()") 
	public void hocelotController(){
		
	}
	
	
	
	@Pointcut("execution(* com.hocelot..*(..))") 
	public void hocelot(){
		
	}
	//|| @annotation(com.hocelot.lib.aspect.ServiceAuditable)"
	@Pointcut("@annotation(org.springframework.web.bind.annotation.RestController) || @annotation(org.springframework.stereotype.Controller)"
			+ "") 
	public void controller(){
		
	}
	
	
	@AllArgsConstructor
	@Getter
	class Before{
		String user;
		String signature;
		Object[] args;
		 
	}
	@AllArgsConstructor
	@Getter
	class After{
		String user;
		String signature;
		Object methodReturn;
		
	}
	@AllArgsConstructor
	@Getter
	class Throws{
		String user;
		String signature;
		String message;
		String value;
		
	}
	static ObjectMapper mapper = new ObjectMapper();
	static {	
		mapper.configure(SerializationFeature.FAIL_ON_EMPTY_BEANS, false);
	}
	
}
