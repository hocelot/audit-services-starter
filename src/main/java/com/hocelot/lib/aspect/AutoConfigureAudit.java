package com.hocelot.lib.aspect;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class AutoConfigureAudit {

	@Bean
	public InOutAspect configureAspect(){
		return new InOutAspect();
	}
}
