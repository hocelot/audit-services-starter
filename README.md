# Audit Service Starter #

Audit I/O rest service (@RestController or CrudRepository of Spring data rest).

how to:

include in 

```javascript
{
  "_links" : {
    "persons" : {
      "href" : "http://localhost:8001/repo/persons{?page,size,sort}",
      "templated" : true
    },
    "profile" : {
      "href" : "http://localhost:8001/repo/profile"
    }
  }
}

```

Swagger



localhost:8001/swagger-ui.html

